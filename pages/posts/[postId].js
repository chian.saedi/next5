

const Post = ({post})=>{

    return(
        <>
            <h1>Post</h1>
            <div>

                <h4>{post.id}</h4>
                <div>{post.title}</div>
                <div>{post.body}</div>
            </div>
        </>
    )
}
export default Post

export async function getStaticPaths(){

    return{
        paths: [
            {
                params: {postId: '1'}
            },
            {
                params: {postId: '2'}
            },
            {
                params: {postId: '3'}
            },

        ],
        fallback: false,
    }
}
export async function getStaticProps(context){

    const {params} = context
    const res = await fetch(`https://jsonplaceholder.typicode.com/posts/${params.postId}`)
    const data = await res.json()
    return{
        props: {
            post: data
        }
    }
}