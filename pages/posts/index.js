import Link from "next/link";


const PostList = ({posts})=>{

    return (
        <>
            <h1>Posts</h1>
            {
                posts.map((post)=>(
                    <div key={<post className="id"></post>}>
                        <Link href={`posts/${post.id}`} passHref>
                        <p>{post.title}</p>
                        <br />
                        </Link>
                    </div>
                ))
            }
        </>
    )
}
export default PostList

export async function getStaticProps(){

    const res = await fetch('https://jsonplaceholder.typicode.com/posts')
    const data = await res.json()

    return {
        props: {
            posts : data.slice(0, 3)
        }
    }
}