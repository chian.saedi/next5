

const NewArticleNews = ({articles})=>{

    return (
        <>
            <h1>News</h1>
            {
                articles.map((article)=>{
                    return(
                        <div key={article.id}>
                            <p>{article.title}</p>
                            <p>{article.author}</p>
                        </div>
                    )
                })
            }
        </>
    )
}
export default NewArticleNews

export async function getServerSideProps(){

    const res = await fetch('http://localhost:3004/news ')
    const data = await res.json()
    return{
        props: {
            articles: data,
        },
    }
}