

const ArticleListByCategory=({articles, category})=>{
    return (
        <>
            <h1> Showing news for category {category} </h1>
                {
                    articles.map((article)=>{
                        return (
                                <div key={article.id}>
                                    <h3>{article.title}</h3>
                                    <h4>{article.description}</h4>
                                    <br />
                                </div>
                        )
                    })
                }

        </>
    )

}

export default ArticleListByCategory


export async function getServerSideProps(context){

    const {params, req, res, query } = context
    console.log(query)
    res.setHeader('Set-Cookie', ['name=Vishwas'])
    console.log()
    const {category} = params
    const response = await fetch(`http://localhost:3004/news?category=${category}`)
    const data = await response.json()

    return {

        props: {
            articles: data,
            category,
        }
    }
}