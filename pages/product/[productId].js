import {useRouter} from "next/router";


const DetailProduct = ()=>{

    const router = useRouter()
    const productId = router.query.productId
    return (
        <>
            <h1>Detaile Product {productId}</h1>
        </>
    )
}
export default DetailProduct