import {useRouter} from "next/router";


const Docs = ()=>{

    const router = useRouter()
    const {params = []} = router.query
    console.log(params)
    if (params.length === 2){
        return <h1>reviewing docs  for { params[0]} and concept {params[1]}</h1>
    }
    if (params.length === 1){

        return <h1>reviewing docs  for { params[0]}</h1>
    }
    return (

        <>
            <h1>Docs Home Pages</h1>
        </>
    )
}
export default Docs